#include "avr/io.h"
#include "avr/interrupt.h"
#include "util/delay.h"

// Пины транзисторов
#define TransistorPIN1 5
#define TransistorPIN2 4
#define TransistorPIN3 3
#define TransistorPIN4 2

#define ColonPIN 6 // двоеточие

// Пины сегментов дисплей
#define NumberPIN1 0
#define NumberPIN2 1
#define NumberPIN3 2
#define NumberPIN4 3
#define NumberPIN5 4
#define NumberPIN6 5
#define NumberPIN7 6

// Пины кнопок
#define MinButtonPIN 0
#define HourButtonPIN 1

uint8_t Seconds = 0;

uint8_t Minutes = 0;
uint8_t MinutesLeft = 0;
uint8_t MinutesRight = 0;

uint8_t Hours = 0;
uint8_t HoursLeft = 0;
uint8_t HoursRight = 0;

uint8_t CurrentSegmentIndex = 0;
uint8_t TickCounter = 0;

// Устанавливаем нужную цифру на дисплее
void SetNumber(uint8_t InNumber)
{
  if (InNumber == 0)
    PORTB = 0b00111111;
  else if (InNumber == 1)
    PORTB = 0b00100100;
  else if (InNumber == 2)
    PORTB = 0b01011101;
  else if (InNumber == 3)
    PORTB = 0b01110101;
  else if (InNumber == 4)
    PORTB = 0b01100110;
  else if (InNumber == 5)
    PORTB = 0b01110011;
  else if (InNumber == 6)
    PORTB = 0b01111011;
  else if (InNumber == 7)
    PORTB = 0b00100101;
  else if (InNumber == 8)
    PORTB = 0b01111111;
  else if (InNumber == 9)
    PORTB = 0b01110111;
}

// Функция увеличения часов
void IncrementHours()
{
  Hours = Hours + 1;
  if (Hours == 24)
  {
    Hours = 0;
  }
}

// Функция увеличения минут
bool IncrementMin()
{
  Minutes = Minutes + 1;
  if (Minutes == 60)
  {
    Minutes = 0;
    return true;
  }
  return false;
}

// Обработчик прерывания по таймеру
ISR(TIMER1_COMPA_vect)
{
  // Выключаем все цифры
  PORTD &= 0b11000011;

  // Мигаем двоеточием считаем время
  TickCounter = TickCounter + 1;
  if (TickCounter == 100)
  {
    PORTD ^= (1 << ColonPIN);
    TickCounter = 0;

    Seconds = Seconds + 1;
    if (Seconds == 120)
    {
      Seconds = 0;
      if (IncrementMin())
      {
        IncrementHours();
      }
    }
  }

  // Динамическая индикация
  CurrentSegmentIndex = CurrentSegmentIndex + 1;
  if (CurrentSegmentIndex == 4)
  {
    CurrentSegmentIndex = 0;
  }

  if (CurrentSegmentIndex == 0)
  {
    HoursLeft = Hours / 10;
    SetNumber(HoursLeft);
    PORTD |= (1 << TransistorPIN1);
  }
  else if (CurrentSegmentIndex == 1)
  {
    HoursRight = Hours;
    while (HoursRight >= 10)
    {
      HoursRight -= 10;
    }

    SetNumber(HoursRight);
    PORTD |= (1 << TransistorPIN2);
  }
  else if (CurrentSegmentIndex == 2)
  {
    MinutesLeft = Minutes / 10;
    SetNumber(MinutesLeft);
    PORTD |= (1 << TransistorPIN3);
  }
  else if (CurrentSegmentIndex == 3)
  {
    MinutesRight = Minutes;
    while (MinutesRight >= 10)
    {
      MinutesRight -= 10;
    }
    SetNumber(MinutesRight);
    PORTD |= (1 << TransistorPIN4);
  }
}

int main()
{
  // включаем пины транзисторов и двоеточия на выход
  DDRD |= (1 << TransistorPIN1) | (1 << TransistorPIN2) | (1 << TransistorPIN3) | (1 << TransistorPIN4) | (1 << ColonPIN);

  // Включаем пины сегментов дисплея на выход
  DDRB = 0b01111111;

  // Включаем подтяжку пинов кнопок
  PORTD |= (1 << MinButtonPIN) | (1 << HourButtonPIN);

  TCCR1B |= (1 << WGM12); // устанавливаем режим СТС (сброс по совпадению)
  TIMSK |= (1 << OCIE1A); // устанавливаем бит разрешения прерывания 1ого счетчика по совпадению с OCR1A

  OCR1A = 0b0001100001101010; // 200Гц - Рабочий вариант

  TCCR1B |= (1 << CS11); // установим делитель на 8

  sei(); // включаем прерывания

  bool bISMinButtonPressed = false;
  bool bISHourButtonPressed = false;

  while (1)
  {
    // кнопка увеличения минут
    if (!(PIND & (1 << MinButtonPIN)) && !bISMinButtonPressed)
    {
      IncrementMin();
      bISMinButtonPressed = true;
      _delay_ms(20);
    }
    else if (PIND & (1 << MinButtonPIN) && bISMinButtonPressed)
    {
      bISMinButtonPressed = false;
      _delay_ms(20);
    }
    //

    // кнопка увеличения часов
    if (!(PIND & (1 << HourButtonPIN)) && !bISHourButtonPressed)
    {
      IncrementHours();
      bISHourButtonPressed = true;
      _delay_ms(20);
    }
    else if (PIND & (1 << HourButtonPIN) && bISHourButtonPressed)
    {
      bISHourButtonPressed = false;
      _delay_ms(20);
    }
  }

  return 0;
}